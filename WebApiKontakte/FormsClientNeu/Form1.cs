﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebApiKontakte.Models;

namespace FormsClientNeu
{
    public partial class Form1 : Form
    {

        private Kontakt kontaktdaten;

        


        public Form1()
        {
            InitializeComponent();

            kontaktdaten = new Kontakt();


            button1.Enabled = false;

            textBox2.Enabled = false;
            comboBox1.Enabled = false;
            comboBox1.Visible = false;

        }



        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                WebClient wc = new WebClient();
                wc.BaseAddress = "http://localhost:53129/api/kontakt/gender/" + textBox1.Text;
                wc.Encoding = Encoding.UTF8;
                string data = wc.DownloadString(wc.BaseAddress);

                textBox2.Text = data;
            }
     
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

            //ComboBox zur Ortsauswahl deaktivieren, falls
            // während der Auswahl neue PLZ eingegeben wird
            comboBox1.Enabled = false;
            comboBox1.Visible = false;

            if (textBox5.Text != "")
            {
                WebClient wc = new WebClient();
                wc.BaseAddress = "http://localhost:53129/api/kontakt/plz/" + textBox5.Text;
                wc.Encoding = Encoding.UTF8;
                string data = wc.DownloadString(wc.BaseAddress);
            
                string ort = data.Replace("\"", "").Replace("\\", "").Replace("[", "").Replace("]", "");
                string[] ortearray = null;

                if (ort.Contains(","))
                {
                    ortearray = ort.Split(',');
                }

                // Verzweigung anhand Inhalt der Variable Ort
                if (ortearray == null)
                {
                    if (ort == "")
                    {
                        ort = "kein Ort zu Plz";
                    }
                }
                else
                {
                    comboBox1.Enabled = true;
                    comboBox1.Visible = true;
                    comboBox1.DataSource = ortearray;

                    ort = ortearray[0];
                }

                // Ausgabe Ort
                textBox6.Text = ort;
            }
      
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string ort = comboBox1.SelectedItem.ToString(); ;
            textBox6.Text = ort;
            comboBox1.Visible = false;
            comboBox1.Enabled = false;
        }




        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
