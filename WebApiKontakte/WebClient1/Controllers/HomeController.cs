﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using WebClient1.Models;

namespace WebClient1.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

       
        [HttpPost]
        public IActionResult Index(string name,string vorname ,string strasse ,string plz)
        {
            WebClient wc = new WebClient();
            wc.BaseAddress = "http://localhost:53129/api/kontakt/" + vorname;
            wc.Encoding = Encoding.UTF8;
            string gender = wc.DownloadString(wc.BaseAddress);

           


            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
