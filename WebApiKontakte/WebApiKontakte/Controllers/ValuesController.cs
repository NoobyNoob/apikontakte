﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApiKontakte.Models;

namespace WebApiKontakte.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            
            return new string[] { "value1", "value2" };
        }

        // testmethode plzabruf
      
        //[HttpGet("plztest/{plz}")]
        //public ActionResult<string> Get(string plz)
        //{
        //    PlzOrtRepo plzortrepo = new PlzOrtRepo();
        //    var result = plzortrepo.GetOrtByPlz(plz);


        //    return new ContentResult { Content = result, ContentType = "application/json" };
        //}

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
