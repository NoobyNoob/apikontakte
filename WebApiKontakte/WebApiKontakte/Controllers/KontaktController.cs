﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApiKontakte.Models;

namespace WebApiKontakte.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class KontaktController : ControllerBase
    {
        private readonly KontaktContext _context;

        NameGenderRepo ngr = new NameGenderRepo();
        PlzOrtRepo plzor = new PlzOrtRepo();

        public KontaktController(KontaktContext context)
        {
            _context = context;
        }

        // GET: api/Kontakt/gender/name
        [HttpGet("gender/{name}")]
        public string GetKontakt(string name)
        {
            //Gender aus API Laden 
            var gender = ngr.GetGenderByName(name);

            //return new ContentResult{ Content = gender , ContentType = "application/json"} ;
            return gender;
        }


     
        // GET: api/Kontakt/plz/58638
        [HttpGet("plz/{plz}")]
        public List<string> GetPlz(string plz)
        {
            //Gender aus API Laden 
            var plzs = plzor.GetOrtByPlz(plz).ToList();
            
            //return new ContentResult{ Content = gender , ContentType = "application/json"} ;
            return plzs;
        }

        //// GET: api/Kontakt/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<Kontakt>> GetKontakt(int id)
        //{
        //    var kontakt = await _context.Kontakt.FindAsync(id);

        //    if (kontakt == null)
        //    {
        //        return NotFound();
        //    }

        //    return kontakt;
        //}

        //// PUT: api/Kontakt/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutKontakt(int id, Kontakt kontakt)
        //{
        //    if (id != kontakt.ID)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(kontakt).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!KontaktExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        //// POST: api/Kontakt
        //[HttpPost]
        //public async Task<ActionResult<Kontakt>> PostKontakt(Kontakt kontakt)
        //{
        //    _context.Kontakt.Add(kontakt);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetKontakt", new { id = kontakt.ID }, kontakt);
        //}

        //// DELETE: api/Kontakt/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Kontakt>> DeleteKontakt(int id)
        //{
        //    var kontakt = await _context.Kontakt.FindAsync(id);
        //    if (kontakt == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Kontakt.Remove(kontakt);
        //    await _context.SaveChangesAsync();

        //    return kontakt;
        //}

        private bool KontaktExists(int id)
        {
            return _context.Kontakt.Any(e => e.ID == id);
        }
    }
}
