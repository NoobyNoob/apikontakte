﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;


namespace WebApiKontakte.Models
{
    public class PlzOrtRepo
    {

        private string url = @"https://api.zippopotam.us";

        private readonly HttpClient httpClient;

       // private string name;

        // Konstruktor
        public PlzOrtRepo()
        {

            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(this.url);
          
            httpClient.DefaultRequestHeaders.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public List<string> GetOrtByPlz(string plz)
        {
            FileInfo fi = new FileInfo(Path.Combine(Directory.GetCurrentDirectory(), "PlzQuerys", plz.ToLower().ToString() + ".json"));

            if (!System.IO.File.Exists(fi.FullName))
            {
                string nameJsonString = GetFromWebApi(plz);

                using (StreamWriter schreibWegDenScheiss = System.IO.File.CreateText(fi.FullName))
                {
                    schreibWegDenScheiss.WriteLine(nameJsonString);
                }
            }

            string jsonStringAusDatei;

            using (FileStream nameJsonAusDatei = System.IO.File.OpenRead(fi.FullName))
            {
                using (var streamReader = new StreamReader(nameJsonAusDatei, Encoding.UTF8))
                {
                    jsonStringAusDatei = streamReader.ReadToEnd();
                }
            }




            // Orte aus Jsonstring 'rausparsen 
            // JsonConvert.DeserializeObject...ToString() baut 
            // einen String aus dem dynamisch objektifiziertem
            // Jason Array, das, -oh Welt der Wunder...-, 
            // als Json.Convert-deserialisiertes dynamisches Objekt alle Sonderzeichen enthält....


            List<string> orte = new List<string>();


            var indexbegin = JsonConvert.DeserializeObject(jsonStringAusDatei).ToString();

            while (indexbegin.Contains("\"place name\""))
            {
                int beginindex = indexbegin.IndexOf("\"place name\"");
                int endindex = indexbegin.IndexOf("\"longitude\"");

                orte.Add(indexbegin.Substring(beginindex + 15, (endindex - 10) - (beginindex + 15)));
                indexbegin = indexbegin.Remove(beginindex, (endindex + 10) - beginindex);
            }





            /////////////////////////////////
            ///
            /// Alternative Lösung:
            /// 
            /// Direktzugriff auf Attribut(e) "places"."place name" 
            /// in JsonConvert-deserialisiertem Json-String
            ///

            List<string> placesListString = new List<string>();


            var dynamicCastedObjectifiedJsonString = JsonConvert.DeserializeObject(jsonStringAusDatei) as dynamic;

            if (dynamicCastedObjectifiedJsonString["places"] != null)
            {
                var placesDynArray = dynamicCastedObjectifiedJsonString["places"];
            
                for (int i = 0; i < placesDynArray.Count; i++)
                {
                    placesListString.Add(placesDynArray[i]["place name"].ToString());
                }
            }
            /////////////////////////////////
            ///
            /// "placesListString" wird nicht weiter verwendet


            ///////////////////
            ///
            /// mehr Spielerei an einer nächsten Alternative
            /// witzige Idee, aber funktioniert noch nicht

            //List<string> OhNoOneMorePlacesListString = new List<string>();

            //string[] testi;

            //testi = dynamicCastedObjectifiedJsonString.ToString()
            //                                          .Split(',');
            //testi = testi.Where(str => str.Contains("place name")).ToArray();


            //for (int i = 0; i < testi.Length; i++)
            //{

            //    var matchcoll =  Regex.Match(testi[i], "^(.+)[:]");

            //    for (int j = 0; j < matchcoll.Groups.Count; j++)
            //    {
            //        testi[i].Replace( matchcoll.Groups[j].ToString(), "");
            //    }                
            //}


            // weil LINQ so schön ist =>

            orte = orte.OrderBy(ort => ort).ToList();

            return orte;           
        }



        //  Holt Von der WebApi-Schnittstelle 'Json' mit Ortsdaten zu gegebener PLZ
        // gibt einen Json-String an aufrufende Methode zurück

        public string GetFromWebApi(string plz)
        {
            HttpResponseMessage responseMessage;

            responseMessage = httpClient.GetAsync("DE/" + plz).Result;


            /// trauriger, stolperhafter Versuch die Sonderzeichen im Json-String vernünftig darzustellen. 

            Encoding iso = Encoding.GetEncoding("ISO-8859-1");
            Encoding utf8 = Encoding.UTF8;
            byte[] utfBytes = utf8.GetBytes(responseMessage.Content.ReadAsStringAsync().Result);
            byte[] isoBytes = Encoding.Convert(utf8, iso, utfBytes);
            string result = iso.GetString(isoBytes);

            //string result = responseMessage.Content.ReadAsStringAsync().Result;

            return result;

        }
    }
}
