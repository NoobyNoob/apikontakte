﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace WebApiKontakte.Models
{


    public class NameGenderRepo
    {
        private string url = @"https://api.genderize.io";

        private readonly HttpClient httpClient;

       // private string name;

       // Konstruktor
        public NameGenderRepo()
        {
           
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(this.url);
            
            httpClient.DefaultRequestHeaders.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public string GetGenderByName(string name)
        {
            FileInfo fi = new FileInfo(Path.Combine(Directory.GetCurrentDirectory(), "NameQuerys", name.ToLower() + ".json"));

            if (!System.IO.File.Exists(fi.FullName))
            {
                string nameJsonString = GetFromWebApi(name);

                using (StreamWriter schreibWegDenScheiss = System.IO.File.CreateText(fi.FullName))
                {
                    schreibWegDenScheiss.WriteLine(nameJsonString);
                }
            }

            string jsonStringAusDatei;

            using (FileStream nameJsonAusDatei = System.IO.File.OpenRead(fi.FullName))
            {
                using (var streamReader = new StreamReader(nameJsonAusDatei, Encoding.UTF8))
                {
                    jsonStringAusDatei = streamReader.ReadToEnd();
                }
            }

            if (jsonStringAusDatei.Contains("female"))
            {
                return "weiblich";
            }
            else if (jsonStringAusDatei.Contains("male"))
            {
                return "männlich";
            }
            else
            {
                return "---";
            }
            
        }


        public string GetFromWebApi(string name)
        {
            HttpResponseMessage responseMessage;

            responseMessage = httpClient.GetAsync("?name=" + name).Result;

            string result = responseMessage.Content.ReadAsStringAsync().Result;

            return result;

        }
    }
 }


