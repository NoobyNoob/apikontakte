﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiKontakte.Models
{
    public partial class Kontakt
    {

        public sealed class KontaktMetaData
        {
            [Key]
            [Display(Name = "KontaktId")]
            public int ID { get; set; }
            [Display(Name = "Nachname")]
            public string Name { get; set; }
            [Display(Name = "Vorname")]
            public string Vorname { get; set; }
            [Display(Name = "Geschlecht")]
            public string Gender { get; set; }
            [Display(Name = "Postleitzahl")]
            public string Plz { get; set; }
            [Display(Name = "Ort")]
            public string Ort { get; set; }
        }



    }
}
