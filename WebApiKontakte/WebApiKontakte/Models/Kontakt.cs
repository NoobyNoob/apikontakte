﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApiKontakte.Models
{
    public partial class Kontakt
    {

        public int ID { get; set; }
        public string Name { get; set; }
        public string Vorname { get; set; }
        public string Gender { get; set; }
        public string Plz { get; set; }
        public string Ort { get; set; }
    }
}
